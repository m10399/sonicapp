package com.example.sonicapp

import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import android.animation.ValueAnimator
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import android.graphics.drawable.AnimationDrawable

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        var foregroundImage1 : ImageView = findViewById(R.id.foreground_image_1)
        var foregroundImage2: ImageView = findViewById(R.id.foreground_image_2)
        val width = 3202
        foregroundImage1.layoutParams.width = width
        foregroundImage2.layoutParams.width = width

        val animator = ValueAnimator.ofFloat(1.0f,0.0f)
        animator.repeatCount = ValueAnimator.INFINITE
        animator.interpolator = LinearInterpolator()
        animator.duration = 12000L
        animator.addUpdateListener { animation ->
            val progress = animation.animatedValue as Float
            val width = foregroundImage1.width
            val translationX = width * progress
            foregroundImage1.setTranslationX(translationX)
            foregroundImage2.setTranslationX(translationX  - width)


        }
        animator.start()
        var sonic : ImageView = findViewById(R.id.sonic)
        sonic.setImageResource(R.drawable.sonic)
        var sonicAnimation : AnimationDrawable = sonic.drawable as AnimationDrawable
        sonicAnimation.start()
    }
}